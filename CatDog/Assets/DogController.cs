﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogController : MonoBehaviour {


    private Animator animator;
    private SpriteRenderer sp;
    private Rigidbody2D rb2d;
    private bool jumping ;
	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        sp = GetComponent<SpriteRenderer>();
        rb2d = GetComponent<Rigidbody2D>();
        jumping = false;
    }
	
	// Update is called once per frame
	void Update () {
        float mov = Input.GetAxis("Horizontal");
        float jump = Input.GetAxis("Vertical");

        if (mov > 0) {
            sp.flipX = false;
            animator.ResetTrigger("idle");
            animator.SetTrigger("walk");
            rb2d.velocity = new Vector2(5f,rb2d.velocity.y);

        }
        if(mov < 0)
        {
            sp.flipX = true;
            animator.ResetTrigger("idle");
            animator.SetTrigger("walk");
            rb2d.velocity = new Vector2(-5f, rb2d.velocity.y);
        }
        if (jump > 0 && !jumping)
        {
            rb2d.AddForce(new Vector2(0f, 300f));
            animator.ResetTrigger("idle");
            animator.ResetTrigger("walk");
            animator.SetTrigger("jump");
            jumping = true;
        }
        if(mov == 0 && jump == 0)
        {
            rb2d.velocity = new Vector2(0, 0);
            animator.ResetTrigger("walk");
            animator.SetTrigger("idle");
        }
	}

    public void jumpStop()
    {
        jumping = false;
    }
}
