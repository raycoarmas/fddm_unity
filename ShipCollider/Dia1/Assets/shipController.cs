﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shipController : MonoBehaviour {

    private Rigidbody2D shipRigidBody;
    private AudioSource sound;
    public GameObject fire;
    private int leftBottonMargin;
    private int rightUpperMargin;
	// Use this for initialization
	void Start () {
        shipRigidBody = GetComponent<Rigidbody2D>();
        sound = GetComponent<AudioSource>();
        fire.SetActive(false);
        leftBottonMargin = 0;
        rightUpperMargin = 1;
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        
        bool advance = Input.GetKey("w") ;
        bool right = Input.GetKey("d");
        bool left = Input.GetKey("a");
        bool down = Input.GetKey("s");
        bool move = false;
        if (advance && !down)
        {
            if (!right && !left)
            {
                shipRigidBody.MoveRotation(0);
                move = true;
            }
        }
        if (left && !right)
        {
            if (left && !advance && !down) {
                shipRigidBody.MoveRotation(90);
            }
            else if (left && down && !advance)
            {
                shipRigidBody.MoveRotation(145);
            }
            else
            {
                shipRigidBody.MoveRotation(45);
            }
            move = true;

        }
        if (right && !left)
        {
            if (right && !advance && !down)
            {
                shipRigidBody.MoveRotation(-90);
            }
            else if (right && down && !advance)
            {
                shipRigidBody.MoveRotation(-145);
            }
            else
            {
                shipRigidBody.MoveRotation(-45);
            }
            move = true;

        }
        if (down && !advance)
        {
            if (!right && !left)
            {
                shipRigidBody.MoveRotation(180);
            }
            move = true;
        }

        if (move)
        {
            activeEffects();
            shipRigidBody.AddForce(transform.up * 20);
        }

        if (!move)
        {
            if (sound.isPlaying) { 
                sound.Stop();
            }
            fire.SetActive(false);
        }

        Vector3 camara = Camera.main.WorldToViewportPoint(transform.position);
        if (camara.x >= rightUpperMargin || camara.x <= leftBottonMargin)
        {
            shipRigidBody.velocity = new Vector2((shipRigidBody.velocity.x *-1)/1.7f, shipRigidBody.velocity.y);
        }
        if (camara.y >= rightUpperMargin || camara.y <= leftBottonMargin)
        {
            shipRigidBody.velocity = new Vector2(shipRigidBody.velocity.x, (shipRigidBody.velocity.y *-1)/1.7f);
        }
    }

    private void activeEffects()
    {
        fire.SetActive(true);
        shipRigidBody.AddForce(transform.up * 20);
        if (!sound.isPlaying)
        {
            sound.Play();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.attachedRigidbody.gameObject.SetActive(false);
    }
}
