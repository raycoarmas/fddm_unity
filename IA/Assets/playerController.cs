﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

    float x;
    float z;
    Vector3 movement;
    Rigidbody rb;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");
        movement.Set(x, 0f, z);
        rb.MovePosition(transform.position + movement);

    }
}
