﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyController : MonoBehaviour {

    public Transform player;

    UnityEngine.AI.NavMeshAgent meshAgent;

	// Use this for initialization
	void Start () {
        meshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();

    }
	
	// Update is called once per frame
	void Update () {
        meshAgent.SetDestination(player.position);

    }
}
