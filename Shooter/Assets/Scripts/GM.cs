﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GM : MonoBehaviour {

    public int enemys;
    public float spawnTime;
    public GameObject enemyObject;

    // Use this for initialization
    void Start () {
		InvokeRepeating("spawn",spawnTime, spawnTime);
	}

    // Update is called once per frame
    void spawn() {
        if (enemys > 0)
        {
            enemys--;
            Vector3 initialPosition = new Vector3(12f, Random.Range(-5f, 5f), 0f);
            Instantiate(enemyObject, initialPosition, Quaternion.identity);
        }


    }
}
