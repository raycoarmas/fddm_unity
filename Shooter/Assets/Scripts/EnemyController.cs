﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    private float timer = 0f;
    private float speed;
    // Use this for initialization
    void Start()
    {
        speed = Random.Range(3, 5);
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        float xPos = timer * speed;

        transform.position = new Vector3(Mathf.Clamp(xPos + 12f, -14f, 14f), transform.position.y, 0f);
        if (transform.position.x <= -12f)
        {
            Destroy(this.gameObject);
        }
    }
}
