﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameController : MonoBehaviour {

    public GameObject enemy;
    public int enemies;
    
    // Use this for initialization
    void Start () {
        InvokeRepeating("spawnEnemy",0.1f,5f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void spawnEnemy()
    {
        if (enemies > 0) {

            GameObject plane = GameObject.Find("Plane");

            float randomX = Random.Range(plane.transform.position.x - plane.transform.localScale.x, plane.transform.position.x + plane.transform.localScale.x);
            float randomZ = Random.Range(plane.transform.position.y - plane.transform.localScale.z, plane.transform.position.y + plane.transform.localScale.z);
            Vector3 randomPosition = new Vector3(randomX, 0.65f, randomZ);
            
            Instantiate(enemy, randomPosition, Quaternion.identity);
            enemies--;
        }
    }
}
