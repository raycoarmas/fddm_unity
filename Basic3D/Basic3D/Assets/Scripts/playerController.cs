﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

    private Rigidbody rb;
    private float speed = 10f;
    private float turnSpeed = 100f;

    private float MovementInputValue;
    private float TurnInputValue;
    private float Fire;

    private bool gotIt = false;
    private GameObject ballCatched;

    private Transform ball;


    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        ball = GameObject.Find("Ball").transform;
    }
	
	// Update is called once per frame
	void Update () {

        MovementInputValue = Input.GetAxis("Vertical1");
        TurnInputValue = Input.GetAxis("Horizontal1");
        Fire = Input.GetAxisRaw("Fire1");

        Move();
        
        if (Fire != 0 && gotIt == true)
        {
            Rigidbody rbBall = ballCatched.GetComponent<Rigidbody>();
            rbBall.isKinematic = false;
            rbBall.transform.SetParent(null);
            rbBall.AddForce(transform.forward * 0.1f);
            gotIt = false;
            ballCatched = null;
        }

        if (gotIt == false){
            /*float x = ball.position.x;
            float z = ball.position.z;
            float y = transform.position.y;*/

            transform.LookAt(ball.position);
        }
        else
        {
            Turn();
        }
    }

    private void FixedUpdate()
    {
       
    }

    private void Move()
    {
        Vector3 movement = transform.forward * MovementInputValue * speed * Time.deltaTime;
        transform.SetPositionAndRotation(transform.position + movement, transform.rotation);
    }


    private void Turn()
    {
        float turn = TurnInputValue * turnSpeed * Time.deltaTime;
        
        Quaternion turnRotation = Quaternion.Euler(transform.rotation.x * turn, transform.rotation.y * turn, transform.rotation.z * turn);
        
        transform.SetPositionAndRotation(transform.position, transform.rotation * turnRotation);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ball" && gotIt == false)
        {
            gotIt = true;
            other.GetComponent<Rigidbody>().isKinematic = true;
            other.transform.SetParent(transform);
            ballCatched = other.gameObject;
        }
    }
}
