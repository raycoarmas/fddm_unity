﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyController : MonoBehaviour {

    private Transform ball;
    //private Rigidbody rb;
    
    private float speed = 1f;
	// Use this for initialization
	void Start () {
        ball = GameObject.Find("Ball").transform;
        //rb = GetComponent<Rigidbody>();
        //GameObject player = GameObject.Find("Player");
        //Physics.IgnoreCollision(GetComponent<Collider>(), player.GetComponent<Collider>(), true);
        //Physics.IgnoreCollision(GetComponent<Collider>(), ball.GetComponent<Collider>(), true);
    }
	
	// Update is called once per frame
	void Update () {
        
        transform.LookAt(ball.position);
        
        Vector3 movement = transform.forward * speed * Time.deltaTime;
        transform.SetPositionAndRotation(transform.position + movement, transform.rotation);

    }
}